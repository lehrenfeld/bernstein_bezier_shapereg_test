## Running the repos content

There are at least three ways you can run the tutorials:
1. Run the tutorials from your host machine which has a recent `ngsxfem` installed with 
``` {.shell}
jupyter notebook bbez.ipynb
```
2. run a docker container with `ngsxfem` and spawn a jupyter server there, e.g. with
``` {.shell}
docker run -p 8888:8888 ngsxfem/ngsxfem-jupyter
```

3. or run `ngsxfem` interactively in the cloud without any local installation through the [binder service](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Flehrenfeld%2Fbernstein_bezier_shapereg_test/HEAD?filepath=bbez.ipynb): 
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.gwdg.de%2Flehrenfeld%2Fbernstein_bezier_shapereg_test/HEAD?filepath=bbez.ipynb)

*  TODO :
  * Module anlegen
  * Beispiele anlegen
  * automatisch generierte/generierbare Dokumentation (pydoc/sphinx) -> gitlab pages
  * automatisches testen
