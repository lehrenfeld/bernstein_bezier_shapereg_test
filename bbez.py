"""
"""

# ------------------------------ LOAD LIBRARIES -------------------------------
from netgen.geom2d import SplineGeometry
from ngsolve import *
from ngsolve.internal import *
from xfem import *
from xfem.lsetcurv import *

ngsglobals.msg_level = 2

# Mesh diameter
maxh = 0.5
# Finite element space order
order = 2
# Stabilization parameter for ghost-penalty

square = SplineGeometry()
square.AddRectangle((-1, -1), (1, 1), bc=1)
ngmesh = square.GenerateMesh(maxh=maxh)
mesh = Mesh(ngmesh)

r = sqrt(x**2 + y**2)
levelset = r - 0.8

# Higher order level set approximation
lsetmeshadap = LevelSetMeshAdaptation(mesh, order=order, threshold=10.1,
                                      discontinuous_qn=True)
deformation = lsetmeshadap.CalcDeformation(levelset)
deformedcoords = CoefficientFunction((x,y)) + deformation
Draw(lsetmeshadap.lset_p1, mesh, "lsetp1")
Draw(deformation, mesh, "deformation")

import numpy as np
import math

og = order

def BernsteinTrig(x, y, i, j, n):
    return math.factorial(n)/math.factorial(i)/math.factorial(j)/math.factorial(n-i-j) \
      * x**i*y**j*(1-x-y)**(n-i-j)
ndtrig = int((og+1)*(og+2)/2)
Bvals = Matrix(ndtrig, ndtrig)
ii = 0
for ix in range(og+1):
    for iy in range(og+1-ix):
        jj = 0
        for jx in range(og+1):
            for jy in range(og+1-jx):
                Bvals[ii,jj] = BernsteinTrig(ix/og, iy/og, jx, jy, og)
                jj += 1
        ii += 1
iBvals_trig = Bvals.I

ipts = [(i/og,j/og) for j in range(og+1) for i in range(og+1-j)]
ir_trig = IntegrationRule(ipts, [0,]*len(ipts))

pts = mesh.MapToAllElements({ET.TRIG: ir_trig}, VOL)

pmat = deformedcoords(pts)
pmat = pmat.reshape(-1, len(ir_trig), 2)
BezierPnts = np.tensordot(iBvals_trig.NumPy(), pmat, axes=(1,1))

import matplotlib.pyplot as plt

l = [3,0,1,2,4,1,3,4,5,3]
for q in range(BezierPnts.shape[1]):
    plt.plot(BezierPnts[l,q,0],BezierPnts[l,q,1], marker = 'o')
plt.show()
