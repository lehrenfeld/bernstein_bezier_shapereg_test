.. Programming Practical documentation master file, created by
   sphinx-quickstart on Wed May 12 13:43:52 2021.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to Programming Practical's documentation!
=================================================

.. automodule:: bbezmodule
	:members:


.. toctree::
   :maxdepth: 2
   :caption: Contents:



Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
