from netgen.geom2d import SplineGeometry
from ngsolve import *
from ngsolve.internal import *
from scipy.special import binom
from xfem import *
from xfem.lsetcurv import *

import math
import numpy as np
import tikzplotlib
import matplotlib.pyplot as plt
