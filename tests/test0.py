from netgen.geom2d import SplineGeometry
from ngsolve import *
from ngsolve.internal import *
from scipy.special import binom
from xfem import *
from xfem.lsetcurv import *

import math
import numpy as np
import tikzplotlib
import matplotlib.pyplot as plt
import sys
import pathlib

path = str(pathlib.Path(__file__).parent.absolute())
sys.path.append(path+'/../python') 
import bbezmodule

order = 3 # fixed for now

square = SplineGeometry()
square.AddRectangle((-1, -1), (1, 1), bc=1)
ngmesh = square.GenerateMesh(maxh=00.85)
mesh = Mesh(ngmesh)
#Draw(mesh)

r = sqrt(x**2 + y**2)
levelset = r - 0.8

lsetmeshadap = LevelSetMeshAdaptation(mesh, order=order, threshold=0.1)
deformation = lsetmeshadap.CalcDeformation(levelset)


bbez = bbezmodule.BbezShapeControl(mesh,deformation)

bbez.Update() # deformation.space.globalorder, mesh.dim 

bbez.Draw() #matplotlib

#does all the magic
bbez.ComputeAngles()
#return min/max angles per element
minangles = bbez.MinAngles()
maxangles = bbez.MaxAngles()

#return true/false - array
#ba_acceptable = bbez.Admissible(minangle, maxangle)
# deformation is adjusted until acceptable
#bbez.Adjust()
bbez.DrawMeshToTikz("mesh.tex")
